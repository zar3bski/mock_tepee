from flask import Flask
from server import *
import pytest
import sys
import os
import pathlib
import unittest
import json
import logging
sys.path.append(os.path.abspath('../server'))

TEST_FOLDER = pathlib.Path(__file__).parent.absolute()
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

"""TEST COMPONENTS"""


class ConfigFactoryTest(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_valid_conf_load(self):
        conf = ConfigFactory("{}/simpleconf.json".format(TEST_FOLDER))
        assert conf.routes['/hello']['GET']['response'] == 'Hello, World!'
        assert len(conf.routes) == 4

    def test_env_loading(self):

        conf = ConfigFactory("{}/simpleconf.json".format(TEST_FOLDER))
        assert conf.env == "development"

        os.environ['FLASK_ENV'] = 'production'
        conf2 = ConfigFactory("{}/simpleconf.json".format(TEST_FOLDER))
        assert conf2.env == "production"

    def test_json_yaml_give_the_same_routes(self):
        conf1 = ConfigFactory("{}/simpleconf.json".format(TEST_FOLDER))
        conf2 = ConfigFactory("{}/simpleconf.yml".format(TEST_FOLDER))

        assert conf1.routes == conf2.routes

    def test_invalid_conf_file_logs_explicitely_and_raise_exception(self):
        with self.assertRaises(SyntaxError):
            conf = ConfigFactory("{}/garbage_conf".format(TEST_FOLDER))
            self.assertTrue(
                "not a valid json nor yaml conf file" in str(self._caplog.records))


class TestBuilding(unittest.TestCase):
    def setUp(self):
        self.conf = ConfigFactory("{}/simpleconf.json".format(TEST_FOLDER))
        self.builder = AppFactory(self.conf)

    def test_view_creator(self):
        self.builder._create_view("/hello")
        self.builder._create_view("/how")
        self.builder._create_view("/are")

        self.assertTrue(callable(self.builder.conf.routes["/hello"]["view"]))
        self.assertTrue(callable(self.builder.conf.routes["/how"]["view"]))
        self.assertTrue(callable(self.builder.conf.routes["/are"]["view"]))

    def test_ressource_creator(self):
        self.builder._create_view("/hello")
        self.builder._create_resource("/hello")
        self.assertTrue('POST' in next(
            self.builder.app.url_map.iter_rules()).methods)

    def test_builder(self):
        app = self.builder.build()
        # TODO

    def test_type_identifier(self):
        payload = MessageProcessor.content_type_identifier('Hello, World!')
        self.assertEqual(
            payload, ('Hello, World!', "text/plain; charset=utf-8"))

        payload = MessageProcessor.content_type_identifier(
            '<status>1</status>')
        self.assertEqual(payload, ('<status>1</status>', "application/xml"))

        payload = MessageProcessor.content_type_identifier({"status": 1})
        self.assertEqual(payload, ('{"status": 1}', "application/json"))


"""TEST APPS"""


class AppTest(unittest.TestCase):

    def setUp(self):
        conf = ConfigFactory("{}/simpleconf.json".format(TEST_FOLDER))
        builder = AppFactory(conf)
        app = builder.build()
        self.c = app.test_client()

    def test_response_and_types(self):
        response = self.c.get("/hello")
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data, b'Hello, World!')

        response = self.c.post("/hello")
        self.assertEqual(201, response.status_code)
        self.assertEqual(response.data, b'')

        response = self.c.post("/how")
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data, b'{"status": 1}')

        response = self.c.post("/are")
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data, b"<status>1</status>")

        response = self.c.post("/you")
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data, b'')

    def test_headers(self):
        response = self.c.get("/hello")
        self.assertEqual(
            response.headers["Content-Type"], "text/plain; charset=utf-8")

        response = self.c.post("/hello")
        self.assertEqual(
            response.headers["Content-Type"], "text/plain; charset=utf-8")

        response = self.c.post("/how")
        self.assertEqual(response.headers["Content-Type"], "application/json")

        response = self.c.post("/are")
        self.assertEqual(response.headers["Content-Type"], "application/xml")

        response = self.c.post("/you")
        self.assertEqual(
            response.headers["Content-Type"], "text/plain; charset=utf-8")

        # TODO: CUSTOM HEADERS
        #self.assertEqual(response.headers["server"], "nginx")


class LoggingTest(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_logging_headers_on_development(self):
        os.environ['FLASK_ENV'] = 'development'
        conf = ConfigFactory("{}/simpleconf.json".format(TEST_FOLDER))
        builder = AppFactory(conf)
        app = builder.build()
        c = app.test_client()
        with self._caplog.at_level(logging.DEBUG) as log:
            response = c.get("/hello", headers={'content-md5': 'some hash'})
            self.assertTrue("Content-Md5" in str(self._caplog.records))
            self.assertTrue("some hash" in str(self._caplog.records))

    def test_logging_hides_headers_in_production(self):
        os.environ['FLASK_ENV'] = 'production'
        conf = ConfigFactory("{}/simpleconf.json".format(TEST_FOLDER))
        builder = AppFactory(conf)
        app = builder.build()
        c = app.test_client()
        with self._caplog.at_level(logging.DEBUG):
            print(str(self._caplog.records))
            response = c.get("/hello", headers={'content-md5': 'some hash'})
            self.assertFalse("Content-Md5" in str(self._caplog.records))
            self.assertFalse("some hash" in str(self._caplog.records))
